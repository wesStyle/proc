package ru.wesstyle.proc.domain.types;

public enum Status {
    Planned,
    InProgress,
    Finished
}
