package ru.wesstyle.proc.domain.types;

public enum Sex {
    Male,
    Female,
    Undefined
}