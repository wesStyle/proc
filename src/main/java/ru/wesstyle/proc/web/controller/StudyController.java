package ru.wesstyle.proc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.wesstyle.proc.domain.Patient;
import ru.wesstyle.proc.domain.Study;
import ru.wesstyle.proc.domain.types.Status;
import ru.wesstyle.proc.service.PatientRepository;
import ru.wesstyle.proc.service.StudyRepository;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api")
public class StudyController {

    @Autowired
    StudyRepository studyRepository;

    @Autowired
    PatientRepository patientRepository;

    @GetMapping("/studies")
    public List<Study> getAllStudies() {
        Study s = new Study();
        Patient p = patientRepository.findById(1l).get();

        s.setDescription("juidesct");
        //s.setPatient(p);
        s.setPlannedStartTime(new Date());
        s.setPlannedEndTime(new Date());
        s.setStatus(Status.Planned);

        studyRepository.save(s);
        return studyRepository.findAll();
    }
}
