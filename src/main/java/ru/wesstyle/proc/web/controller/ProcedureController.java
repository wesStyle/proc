package ru.wesstyle.proc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.wesstyle.proc.domain.Procedure;
import ru.wesstyle.proc.domain.types.Status;
import ru.wesstyle.proc.service.ProcedureService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ProcedureController {

    @Autowired
    ProcedureService procedureService;

    @GetMapping("/procedures")
    public List<Procedure> getAllProcedures() {
        return procedureService.getAllProcedures();
    }

    @PostMapping("/procedures")
    public Procedure createProcedure(@Valid @RequestBody Procedure procedure) {
        return procedureService.scheduleProcedure(procedure);
    }

    @PutMapping("/procedures/{id}")
    public Procedure updateProcedureStatus(@PathVariable(value = "id") Long id, @Valid @RequestBody Status newStatus) {
        return procedureService.updateProcedureStatus(id, newStatus);
    }
}
