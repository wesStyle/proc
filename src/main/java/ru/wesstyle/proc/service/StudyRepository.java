package ru.wesstyle.proc.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.wesstyle.proc.domain.Study;

@Repository
public interface StudyRepository extends JpaRepository<Study, Long> {
}
