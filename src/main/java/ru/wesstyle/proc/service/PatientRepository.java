package ru.wesstyle.proc.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.wesstyle.proc.domain.Patient;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {
}
