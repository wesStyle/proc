package ru.wesstyle.proc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.wesstyle.proc.domain.Doctor;
import ru.wesstyle.proc.domain.Procedure;
import ru.wesstyle.proc.domain.Room;
import ru.wesstyle.proc.domain.Study;
import ru.wesstyle.proc.domain.types.Status;
import ru.wesstyle.proc.exception.ProcedureScheduleFailureException;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * {@inheritDoc}
 */
@Service
public class ProcedureServiceImpl implements ProcedureService {

    @Autowired
    ProcedureRepository procedureRepository;

    @Autowired
    DoctorRepository doctorRepository;

    @Autowired
    RoomRepository roomRepository;

    @Autowired
    StudyRepository studyRepository;

    @Override
    public List<Procedure> getAllProcedures() {
        return procedureRepository.findAll();
    }

    /**
     * {@inheritDoc}
     * <p>
     * While scheduling new Procedure there are some things that should be taken into account:
     * - If new Study has End time planned then it shouldn't be before or the same as Start time.
     * - If new Study Start/End times are interfering with existing Procedure with the same Doctor, Room or Study Patient time periods
     */
    @Override
    @Transactional
    public Procedure scheduleProcedure(Procedure procedure) {

        Optional<Doctor> d = doctorRepository.findById(procedure.getDoctor().getId());
        if (!d.isPresent())
            throw new EntityNotFoundException("Doctor not found");
        Optional<Room> r = roomRepository.findById(procedure.getRoom().getId());
        if (!r.isPresent())
            throw new EntityNotFoundException("Room not found");

        Study s = procedure.getStudy();
        Optional<Date> studyEndTime = Optional.ofNullable(s.getPlannedEndTime());
        if (studyEndTime.isPresent() &&
                (studyEndTime.get().before(s.getPlannedStartTime()) ||
                        studyEndTime.get().equals(s.getPlannedStartTime())))
            throw new ProcedureScheduleFailureException("Study end time is equal or before Start time");


        List<Procedure> procs = procedureRepository.findAll();

        for (Procedure p : procs) {
            if (p.getDoctor().getId().equals(d.get().getId())) {
                Study study2 = p.getStudy();
                if (isStudyCollapsing(s, study2))
                    throw new ProcedureScheduleFailureException("Doctor is busy at the chosen time");
            }
            if (p.getRoom().getId().equals(r.get().getId())) {
                Study study2 = p.getStudy();
                if (isStudyCollapsing(s, study2))
                    throw new ProcedureScheduleFailureException("Room is busy at the chosen time");
            }
            if (p.getStudy().getPatient().getId().equals(s.getPatient().getId())) {
                Study study2 = p.getStudy();
                if (isStudyCollapsing(s, study2))
                    throw new ProcedureScheduleFailureException("Patient is busy at the chosen time");
            }
        }

        Procedure p = new Procedure();
        p.setDoctor(d.get());
        p.setRoom(r.get());
        p.setStudy(s);

        return procedureRepository.save(p);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Procedure updateProcedureStatus(Long id, Status newStatus) {
        Optional<Procedure> p = procedureRepository.findById(id);
        if (!p.isPresent())
            throw new EntityNotFoundException("Procedure not found");

        p.get().getStudy().setStatus(newStatus);
        return procedureRepository.save(p.get());
    }

    private boolean isStudyCollapsing(Study s1, Study s2) {
        Optional<Date> s1End, s2End;
        s1End = Optional.ofNullable(s1.getPlannedEndTime());
        s2End = Optional.ofNullable(s2.getPlannedEndTime());

        // Both studies have End time set [] []
        if (s1End.isPresent() && s2End.isPresent()) {
            // New Study time period is inside existing Study period
            if (s1.getPlannedStartTime().after(s2.getPlannedStartTime()) &
                    s1.getPlannedStartTime().before(s2.getPlannedEndTime()))
                return true;
            if (s1.getPlannedEndTime().after(s2.getPlannedStartTime()) &
                    s1.getPlannedEndTime().before(s2.getPlannedEndTime()))
                return true;

            // Existing study period is inside or equal new Study time period
            if (s1.getPlannedStartTime().before(s2.getPlannedStartTime()) &
                    s1.getPlannedEndTime().after(s2.getPlannedEndTime()) ||
                    s1.getPlannedStartTime().equals(s2.getPlannedStartTime()) ||
                    s1.getPlannedEndTime().equals(s2.getPlannedEndTime()))
                return true;
        }

        // New Study doesn't have a set End time but existing Study has End time [ []
        if (!s1End.isPresent() && s2End.isPresent()) {
            if (s1.getPlannedStartTime().equals(s2.getPlannedStartTime()) ||
                    s1.getPlannedStartTime().equals(s2.getPlannedEndTime()) ||
                    (s1.getPlannedStartTime().after(s2.getPlannedStartTime()) && s1.getPlannedStartTime().before(s2.getPlannedEndTime())))
                return true;

        }

        // New Study and existing Study don't have a set End time [ [
        if (!s1End.isPresent() && !s2End.isPresent()) {
            if (s1.getPlannedStartTime().equals(s2.getPlannedStartTime()))
                return true;
        }

        // New study has a set End time and existing Study doesn't have a set End time [] [
        if (s1End.isPresent() && !s2End.isPresent()) {
            if (s2.getPlannedStartTime().equals(s1.getPlannedStartTime()) ||
                    s2.getPlannedStartTime().equals(s1.getPlannedEndTime()) ||
                    (s2.getPlannedStartTime().after(s1.getPlannedStartTime()) && s2.getPlannedStartTime().before(s1.getPlannedEndTime())))
                return true;
        }

        return false;
    }
}
