package ru.wesstyle.proc.service;

import ru.wesstyle.proc.domain.Procedure;
import ru.wesstyle.proc.domain.types.Status;

import java.util.List;

/**
 * Service to get, add and modify Procedures.
 */
public interface ProcedureService {

    /**
     * @return all scheduled procedures
     */
    List<Procedure> getAllProcedures();

    /**
     * Schedules a new Procedure
     *
     * @param procedure Procedure to be scheduled
     * @return scheduled Procedure
     */
    Procedure scheduleProcedure(Procedure procedure);

    /**
     * @param id        Procedure id
     * @param newStatus new Procedure Status
     * @return Procedure with changed Status
     */
    Procedure updateProcedureStatus(Long id, Status newStatus);
}
