package ru.wesstyle.proc.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.wesstyle.proc.domain.Doctor;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {
}
