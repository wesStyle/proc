package ru.wesstyle.proc.exception;

import ru.wesstyle.proc.domain.Procedure;

/**
 * Exception thrown during
 *
 * @see ru.wesstyle.proc.service.ProcedureService#scheduleProcedure(Procedure)  calls
 */
public class ProcedureScheduleFailureException extends RuntimeException {
    public ProcedureScheduleFailureException(String message) {
        super(message);
    }
}
