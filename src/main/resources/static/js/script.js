var patients;
var doctors;
var rooms;
var procedures;

$( document ).ready(function() {
    // Init all collections
    refreshPatients();
    refreshDoctors();
    refreshRooms();
    refreshProcedures();


    //---------------------------------
    $('#patientListRefreshBtn').click(function (e) {
        refreshPatients();
    });
    $('#procedureListRefreshBtn').click(function (e) {
        refreshProcedures();
    });

    // Status change call
    $('#changeStatusBtn').click(function (e) {
        var procId = $("input[name='procRadio']:checked").val();
        if (!procId) {
            showAlert('Please select procedure from the table', true);
            return;
        }
        var newStatus = $('#statusSelNew').val();

        $.ajax({
            type: 'PUT',
            contentType: 'application/json',
            url: 'api/procedures/' + procId,
            data: JSON.stringify(newStatus),
            dataType: 'json',
            timeout: 600000,
            success: function (data) {
                refreshProcedures();
                showAlert('Procedure status changed.');
            },
            error: function (e) {
                if (e.responseJSON && e.responseJSON.message)
                    showAlert(e.responseJSON.message, true);
                else
                    showAlert('',true);
            }
        });

    });

    // Add patient call
    $('#patientAddBtn').click(function(e) {
        var patient = {};

        patient.name = $('#nameInp').val();
        if (patient.name == null || patient.name == '') {
            showAlert('Patient name must not be blank!', true);
            return;
        } else if (patient.name.length > 40) {
            showAlert('Description length must be less than 40!', true);
            return;
        }

        patient.sex = $('#sexSel').val();
        if (patient.sex == '')
            patient.sex = null;
        patient.birthDate = $('#birthDateInp').val();
        if (patient.birthDate != null && patient.birthDate !='') {
            var date = moment(patient.birthDate);
            if (!date.isValid()) {
                showAlert('Birth date is in a wrong format!', true);
                return;
            }
        }

        console.log(patient);

        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: 'api/patients',
            data: JSON.stringify(patient),
            dataType: 'json',
            timeout: 600000,
            success: function (data) {
                refreshPatients();
                showAlert('Patient added.');
            },
            error: function (e) {
                if (e.responseJSON && e.responseJSON.message)
                    showAlert(e.responseJSON.message, true);
                else
                    showAlert('',true);
            }
        });
    });


    // Add procedure call
    $('#procedureAddBtn').click(function(e) {
        var procedure = {};
        var study = {};

        study.description = $('#descInp').val();
        if (study.description == null || study.description == '') {
            showAlert('Description must not be blank!', true);
            return;
        } else if (study.description.length > 200) {
            showAlert('Description length must be less than 200!', true);
            return;
        }

        study.patient = $('#patientSel option:selected').data();
        if (!study.patient) {
            showAlert('Patient must be choosen', true);
            return;
        }

        study.status = $('#statusSel').val();
        if (study.status == '')
            study.status = null;

        study.plannedStartTime = $('#startTimeInp').val();
        if (study.plannedStartTime != null && study.plannedStartTime !='') {
            var date = moment(study.plannedStartTime);
            if (!date.isValid()) {
                showAlert('Planned start time is in a wrong format!', true);
                return;
            }
            study.plannedStartTime = date.toISOString();
        } else if (!study.plannedStartTime) {
            showAlert('Planned start time must not be blank!', true);
            return;
        }

        study.plannedEndTime = $('#endTimeInp').val();
        if (study.plannedEndTime != null && study.plannedEndTime !='') {
            var date = moment(study.plannedEndTime);
            if (!date.isValid()) {
                showAlert('Planned start time is in a wrong format or missing!', true);
                return;
            }
            study.plannedEndTime = date.toISOString();
        }

        procedure.doctor = $('#docSel option:selected').data();
        if (!procedure.doctor) {
            showAlert('Doctor must be choosen', true);
            return;
        }

        procedure.room = $('#roomSel option:selected').data();
        if (!procedure.room) {
            showAlert('Room must be choosen', true);
            return;
        }

        procedure.study = study;

        console.log(procedure);

        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: 'api/procedures',
            data: JSON.stringify(procedure),
            dataType: 'json',
            timeout: 600000,
            success: function (data) {
                refreshProcedures();
                showAlert('Procedure scheduled.');
            },
            error: function (e) {
                if (e.responseJSON && e.responseJSON.message)
                    showAlert(e.responseJSON.message, true);
                else
                    showAlert('',true);
            }
        });
    });
});


// Show new dismissible alert
function showAlert(text, isError) {
    if (isError) {
        $('#alertArea').append('<div class="alert alert-danger alert-dismissible fade show" role="alert">\n' +
            '            <strong>Error occured</strong> ' + text + '\n' +
            '            <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
            '                <span aria-hidden="true">&times;</span>\n' +
            '            </button>\n' +
            '        </div>\n');
    } else {
        $('#alertArea').append('<div class="alert alert-success alert-dismissible fade show" role="alert">\n' +
            '            <strong>Success</strong> ' + text + '\n' +
            '            <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
            '                <span aria-hidden="true">&times;</span>\n' +
            '            </button>\n' +
            '        </div>\n');
    }
}

// Get all procedures call
function refreshProcedures() {
    $.ajax({
        url: "api/procedures"
    }).then(function (data) {
        procedures = data;
        console.log(procedures);
        $('#procedureTable').empty();
        $.each(data, function (i, v) {
            $('#procedureTable').append('<tr>');

            $('#procedureTable').append('<td><input class="form-check-input" type="radio" name="procRadio" value="' + v.id + '" style="margin-left: 0px;"/></td></td>');

            $('#procedureTable').append('<td>' + v.study.patient.name + '</td>');
            $('#procedureTable').append('<td>' + v.study.description + '</td>');
            $('#procedureTable').append('<td>' + v.study.status + '</td>');
            $('#procedureTable').append('<td>' + moment(v.study.plannedStartTime).format('YYYY-MM-DD H:m') + '</td>');
            $('#procedureTable').append('<td>' + (v.study.plannedEndTime == null ? '' : moment(v.study.plannedEndTime).format('YYYY-MM-DD H:m')) + '</td>');
            $('#procedureTable').append('<td>' + v.doctor.name + '</td>');
            $('#procedureTable').append('<td>' + v.room.name + '</td>');

            $('#procedureTable').append('</tr>');
        }, function (e) {
            if (e.responseJSON && e.responseJSON.message)
                showAlert(e.responseJSON.message, true);
            else
                showAlert('', true);
        });
    });
}

// Get all doctors call
function refreshDoctors() {
    $.ajax({
        url: 'api/doctors'
    }).then(function(data) {
        doctors = data;
        console.log(doctors);
        $('#docSel').empty();
        $.each(data, function(i, v) {
            // Because Doctors uniqueness is determined by their Id - adding it to the option.
            $('#docSel').append(
                $('<option />')
                    .text(v.name + ' (Id: ' + v.id + ')')
                    .data(v)
            );
        });
    }, function (e) {
        if (e.responseJSON && e.responseJSON.message)
            showAlert(e.responseJSON.message, true);
        else
            showAlert('',true);
    });
}


// Get all rooms call
function refreshRooms() {
    $.ajax({
        url: 'api/rooms'
    }).then(function(data) {
        rooms = data;
        console.log(rooms);
        $('#roomSel').empty();
        $.each(data, function(i, v) {
            // Because Rooms uniqueness is determined by their Id - adding it to the option.
            $('#roomSel').append(
                $('<option />')
                    .text(v.name + ' (Id: ' + v.id + ')')
                    .data(v)
            );
        });
    }, function (e) {
        if (e.responseJSON && e.responseJSON.message)
            showAlert(e.responseJSON.message, true);
        else
            showAlert('',true);
    });
}


// Get all patients call
function refreshPatients() {
    $.ajax({
        url: 'api/patients'
    }).then(function(data) {
        patients = data;
        console.log(patients);
        $('#patientTable').empty();
        $('#patientSel').empty();
        $.each(data, function(i, v) {

            // Because Patients uniqueness is determined by their Id - adding it to the option.
            $('#patientSel').append(
                $('<option />')
                    .text(v.name + ' (Id: ' + v.id + ')')
                    .data(v)
            );

            $('#patientTable').append('<tr>');
            $('#patientTable').append('<td>' + v.id + '</td>');
            $('#patientTable').append('<td>' + v.name + '</td>');
            $('#patientTable').append('<td>' + (v.sex == null ? '': v.sex ) + '</td>');
            $('#patientTable').append('<td>' + (v.birthDate == null ? '' : v.birthDate ) + '</td>');
            $('#patientTable').append('</tr>');
        }, function (e) {
            if (e.responseJSON && e.responseJSON.message)
                showAlert(e.responseJSON.message, true);
            else
                showAlert('',true);
        });
    });
}