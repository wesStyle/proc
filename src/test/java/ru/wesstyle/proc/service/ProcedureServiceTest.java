package ru.wesstyle.proc.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.wesstyle.proc.domain.*;
import ru.wesstyle.proc.domain.types.Sex;
import ru.wesstyle.proc.domain.types.Status;
import ru.wesstyle.proc.exception.ProcedureScheduleFailureException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


/*
    Integration tests of Procedure managing service layer.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProcedureServiceTest {

    @Autowired
    private ProcedureService procedureService;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private ProcedureRepository procedureRepository;

    @Test
    public void procedureScheduleTest() throws ParseException {
        // Creating new Doctor, Room and Study for the Procedure
        Doctor d = new Doctor("Dr. House");
        Doctor doctor = doctorRepository.save(d);

        Room r = new Room("Room 777");
        Room room = roomRepository.save(r);

        Patient p = new Patient();
        p.setSex(Sex.Male);
        p.setName("Forman");
        p.setBirthDate(new Date());
        Patient patient = patientRepository.save(p);

        Study s = new Study();
        s.setStatus(Status.Planned);
        s.setDescription("Test study");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        s.setPlannedStartTime(sdf.parse("13/02/2018 12:30:00"));
        s.setPlannedEndTime(sdf.parse("13/02/2018 14:45:00"));
        s.setPatient(patient);

        Procedure proc = new Procedure();
        proc.setStudy(s);
        proc.setRoom(room);
        proc.setDoctor(doctor);

        // Scheduling a new Procedure
        Procedure scheduledProc = procedureService.scheduleProcedure(proc);

        // Procedure was scheduled with correct id
        assertThat(scheduledProc).isNotNull();
        assertThat(scheduledProc.getId()).isPositive();

        // Finding scheduled procedure by id
        Optional<Procedure> foundProc = procedureRepository.findById(scheduledProc.getId());

        // Scheduled procedure was found
        assertThat(foundProc).isPresent();


        // Scheduled procedure has all the intended fields set correctly
        Procedure foundProcPresent = foundProc.get();
        assertThat(foundProcPresent.getDoctor()).isEqualToIgnoringGivenFields(doctor, "id");
        assertThat(foundProcPresent.getRoom()).isEqualToIgnoringGivenFields(room, "id");
        Study foundStudy = foundProcPresent.getStudy();
        assertThat(foundStudy.getPatient()).isEqualToIgnoringGivenFields(patient, "id", "birthDate");
        assertThat(foundStudy.getPatient().getBirthDate()).isEqualToIgnoringHours(patient.getBirthDate());
        assertThat(foundStudy.getStatus()).isEqualByComparingTo(s.getStatus());
        assertThat(foundStudy.getPlannedEndTime()).hasSameTimeAs(s.getPlannedEndTime());
        assertThat(foundStudy.getPlannedStartTime()).hasSameTimeAs(s.getPlannedStartTime());
        assertThat(foundStudy.getDescription()).isEqualTo(s.getDescription());
    }

    @Test
    public void procedureScheduleExceptionsTest() throws ParseException {
        // Creating new Doctor, Room and Study for the Procedure
        Doctor d = new Doctor("Dr. House");
        Doctor doctor = doctorRepository.save(d);

        Room r = new Room("Room 777");
        Room room = roomRepository.save(r);

        Patient p = new Patient();
        p.setSex(Sex.Male);
        p.setName("Forman");
        p.setBirthDate(new Date());
        Patient patient = patientRepository.save(p);

        Study s = new Study();
        s.setStatus(Status.Planned);
        s.setDescription("Test study");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        s.setPlannedStartTime(sdf.parse("13/02/2018 12:30:00"));
        s.setPlannedEndTime(sdf.parse("13/02/2018 14:45:00"));
        s.setPatient(patient);

        Procedure proc = new Procedure();
        proc.setStudy(s);
        proc.setRoom(room);
        proc.setDoctor(doctor);

        // Scheduling a new Procedure
        Procedure scheduledProc = procedureService.scheduleProcedure(proc);

        Room r2 = new Room("Room 555");
        Room room2 = roomRepository.save(r2);

        Patient p2 = new Patient();
        p2.setSex(Sex.Male);
        p2.setName("Freeman");
        p2.setBirthDate(new Date());
        Patient patient2 = patientRepository.save(p2);

        Study s2 = new Study();
        s2.setStatus(Status.Planned);
        s2.setDescription("Test study 2");

        // Start time > Start time of the previous study
        // End time > End time of the previous study
        s2.setPlannedStartTime(sdf.parse("13/02/2018 12:40:00"));
        s2.setPlannedEndTime(sdf.parse("13/02/2018 17:40:00"));
        s2.setPatient(patient2);

        Procedure proc2 = new Procedure();
        proc2.setStudy(s2);
        proc2.setRoom(room2);
        proc2.setDoctor(doctor);

        // Since this Doctor already has a Study scheduled at this time period, throw exception
        assertThatThrownBy(() -> procedureService.scheduleProcedure(proc2)).isInstanceOf(ProcedureScheduleFailureException.class).hasMessageContaining("Doctor");

        Patient p3 = new Patient();
        p3.setSex(Sex.Female);
        p3.setName("Clarissa");
        p3.setBirthDate(new Date());
        Patient patient3 = patientRepository.save(p3);

        Study s3 = new Study();
        s3.setStatus(Status.Planned);
        s3.setDescription("Test study 3");

        // Start time < Start time of the previous study
        // End time > Start time of the previous study
        s3.setPlannedStartTime(sdf.parse("13/02/2018 11:40:00"));
        s3.setPlannedEndTime(sdf.parse("13/02/2018 12:40:00"));
        s3.setPatient(patient3);

        Doctor d2 = new Doctor("Dr. Frankenstein");
        Doctor doctor2 = doctorRepository.save(d2);

        Procedure proc3 = new Procedure();
        proc3.setStudy(s3);
        proc3.setRoom(room);
        proc3.setDoctor(doctor2);

        // Since this Room already has a Study scheduled at this time period, throw exception
        assertThatThrownBy(() -> procedureService.scheduleProcedure(proc3)).isInstanceOf(ProcedureScheduleFailureException.class).hasMessageContaining("Room");

        Study s4 = new Study();
        s4.setStatus(Status.Planned);
        s4.setDescription("Test study 4");

        // Start time < Start time of the previous study
        // End time > End time of the previous study
        s4.setPlannedStartTime(sdf.parse("13/02/2018 11:00:00"));
        s4.setPlannedEndTime(sdf.parse("13/02/2018 17:30:00"));
        s4.setPatient(patient);

        Procedure proc4 = new Procedure();
        proc4.setStudy(s4);
        proc4.setRoom(room2);
        proc4.setDoctor(doctor2);

        // Since this Patient already has a Study scheduled at this time period, throw exception
        assertThatThrownBy(() -> procedureService.scheduleProcedure(proc4)).isInstanceOf(ProcedureScheduleFailureException.class).hasMessageContaining("Patient");


        // -- Testing of different Start/End time combinations --

        // Start time == Start time of the previous study
        // End time > End time of the previous study
        proc4.getStudy().setPlannedStartTime(sdf.parse("13/02/2018 12:30:00"));
        // Since this Patient already has a Study scheduled at this time period, throw exception
        assertThatThrownBy(() -> procedureService.scheduleProcedure(proc4)).isInstanceOf(ProcedureScheduleFailureException.class).hasMessageContaining("Patient");

        // Start time < Start time of the previous study
        // End time == End time of the previous study
        proc4.getStudy().setPlannedStartTime(sdf.parse("13/02/2018 12:00:00"));
        proc4.getStudy().setPlannedEndTime(sdf.parse("13/02/2018 14:45:00"));
        // Since this Patient already has a Study scheduled at this time period, throw exception
        assertThatThrownBy(() -> procedureService.scheduleProcedure(proc4)).isInstanceOf(ProcedureScheduleFailureException.class).hasMessageContaining("Patient");

        // End time < Start time
        proc4.getStudy().setPlannedStartTime(sdf.parse("13/02/2018 12:00:00"));
        proc4.getStudy().setPlannedEndTime(sdf.parse("13/02/2018 11:45:00"));
        // Since Start/End time pair is st incorrectly, throw error
        assertThatThrownBy(() -> procedureService.scheduleProcedure(proc4)).isInstanceOf(ProcedureScheduleFailureException.class).hasMessageContaining("Study end time is equal or before Start time");

        // End time = Start time
        proc4.getStudy().setPlannedStartTime(sdf.parse("13/02/2018 12:00:00"));
        proc4.getStudy().setPlannedEndTime(sdf.parse("13/02/2018 12:00:00"));
        // Since Start/End time pair is st incorrectly, throw error
        assertThatThrownBy(() -> procedureService.scheduleProcedure(proc4)).isInstanceOf(ProcedureScheduleFailureException.class).hasMessageContaining("Study end time is equal or before Start time");


        proc4.getDoctor().setId(100l);
        // Since Doctor id isn't currently presented in the database, throw exception
        assertThatThrownBy(() -> procedureService.scheduleProcedure(proc4)).hasMessageContaining("Doctor not found");

        proc4.setDoctor(doctor);
        proc4.getRoom().setId(100l);
        // Since Room id isn't currently presented in the database, throw exception
        assertThatThrownBy(() -> procedureService.scheduleProcedure(proc4)).hasMessageContaining("Room not found");
    }

    @Test
    public void procedureChangeStatusTest() throws ParseException {
        // Creating new Doctor, Room and Study for the Procedure
        Doctor d = new Doctor("Dr. House");
        Doctor doctor = doctorRepository.save(d);

        Room r = new Room("Room 777");
        Room room = roomRepository.save(r);

        Patient p = new Patient();
        p.setSex(Sex.Male);
        p.setName("Forman");
        p.setBirthDate(new Date());
        Patient patient = patientRepository.save(p);

        Study s = new Study();

        // Initial status - Planned
        s.setStatus(Status.Planned);
        s.setDescription("Test study");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        s.setPlannedStartTime(sdf.parse("13/02/2018 12:30:00"));
        s.setPlannedEndTime(sdf.parse("13/02/2018 14:45:00"));
        s.setPatient(patient);

        Procedure proc = new Procedure();
        proc.setStudy(s);
        proc.setRoom(room);
        proc.setDoctor(doctor);

        // Scheduling a new Procedure
        Procedure scheduledProc = procedureService.scheduleProcedure(proc);

        Procedure updatedProc = procedureService.updateProcedureStatus(scheduledProc.getId(), Status.InProgress);

        // Status changed to InProgress
        assertThat(updatedProc.getStudy().getStatus()).isEqualByComparingTo(Status.InProgress);

        updatedProc = procedureService.updateProcedureStatus(scheduledProc.getId(), Status.Finished);

        // Status changed to Finished
        assertThat(updatedProc.getStudy().getStatus()).isEqualByComparingTo(Status.Finished);

        // Since provided id isn't currently presented in the database, throw exception
        assertThatThrownBy(() -> procedureService.updateProcedureStatus(100l, Status.Planned)).hasMessageContaining("Procedure not found");
    }
}
