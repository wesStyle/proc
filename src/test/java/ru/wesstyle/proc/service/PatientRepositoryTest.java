package ru.wesstyle.proc.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import ru.wesstyle.proc.domain.Patient;
import ru.wesstyle.proc.domain.types.Sex;

import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/*
    Since adding patients is done through a Spring Data repository, test it.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class PatientRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PatientRepository patientRepository;

    @Test
    public void patientAddTest() {
        Patient p = new Patient();
        p.setName("John");
        p.setBirthDate(new Date());
        p.setSex(Sex.Male);

        entityManager.persist(p);
        entityManager.flush();

        Patient added = patientRepository.save(p);
        Optional<Patient> found = patientRepository.findById(added.getId());

        assertThat(added).isEqualToIgnoringGivenFields(p, "id");
        assertThat(found).isPresent().get().isEqualToComparingFieldByField(added);
    }
}
