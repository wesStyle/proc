package ru.wesstyle.proc.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.wesstyle.proc.domain.Doctor;
import ru.wesstyle.proc.domain.Procedure;
import ru.wesstyle.proc.service.ProcedureService;
import ru.wesstyle.proc.web.controller.ProcedureController;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/*
    Simple controller test to check correct status and data type regardless of service implementation.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(ProcedureController.class)
public class ProcedureControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProcedureService service;

    @Test
    public void GetAllProceduresJsonArrayTest()
            throws Exception {
        Procedure p = new Procedure();
        p.setDoctor(new Doctor("Dr. House"));
        List<Procedure> allProcedures = Arrays.asList(p);
        given(service.getAllProcedures()).willReturn(allProcedures);

        mvc.perform(get("/api/procedures")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].doctor.name", is(p.getDoctor().getName())));
    }
}
