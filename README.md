# Procedure Scheduling Web App

A study scheduling application in which procedures for treatment of patients performed by doctors are planned.

The application has 3 use cases:
* Adding new patients
* Scheduling new procedures
* Updating status of procedure

## Prerequisites

```
Java 8
Internet connection
```

## Running the application

To run the web application using Spring Boot

```
mvn spring-boot:run
```

## Running the tests

To run unit and integration tests

```
mvn test
```